/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

/**
 * http is a node-specific basic web server that I am piggy backing off of.
 */
var server = require('http').createServer(),
/**
 * Require the socket.io module and tell it to use the http server
 */
io = require('socket.io')(server),
/**
 * A nice logging tool
 */
log = require('log4js').getLogger('[server]');

/**
 * Handle a socket that has just connected.
 */
io.on('connection', function (socket) {
    log.info('client connected');

    /**
     * Handle a client socket trying to join a room.
     */
    socket.on('join', function (room) {
        log.info('client joined room ' + room);

        /**
         * Join the socket to the specified room.
         */
        socket.join(room);

        /**
         * Now start listening for the 'send' event emitted by the client
         * when it sends a message.
         */
        socket.on('send', function (message) {
            log.info('client sent message "' + message + '" to room ' + room);

            /**
             * Broadcast the 'message' even to the sender and to the room it
             * belongs to.
             */
            socket.to(room).emit('message', message);
            socket.emit('message', message);
        });
    });
});

/**
 * Start listening for connections on port 9999
 */
log.info('server started');
server.listen(9999);