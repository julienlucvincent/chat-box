/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

import io from 'socket.io-client'

class client {

    constructor() {

        this.socket = null;
        this.listener = () => {
        };
    }

    /**
     * Attempt to connect to a socket on port 9999,
     * on connection emit the 'join' event with a room
     * you would like to join.
     *
     * @param room - the room to join.
     */
    connect(room) {
        this.socket = io(window.location.hostname + ':9999');

        this.socket.emit('join', room);
        this.listen();
    }

    /**
     * Send a message to the server socket, where it will
     * be broad casted to everyone in the same room.
     *
     * @param message - the message to send.
     */
    send(message) {
        if (this.socket) {
            this.socket.emit('send', message)
        }
    }

    /**
     * Register a callback that will be run when the
     * server emits a 'message' event.
     *
     * A message will be passed as the first argument to
     * this callback.
     *
     * @param cb - the callback as a function that will be run.
     */
    register(cb) {
        this.listener = cb;
    }

    /**
     * listen for when the server emits the 'message' event
     * and run the registered callback when the event is triggered.
     */
    listen() {
        this.socket.on('message', this.listener)
    }
}

let run = new client();
export default run;