/*
 |--------------------------------------------------------------------------
 | Serve web-server
 |--------------------------------------------------------------------------
 **/

var serve = require("storm-serve"),
    express = require("express"),
    server = express(),
    compression = require('compression'),
    path = require('path'),
    servePort = 8000;

var serve_conf = {
    mappings: {
        "/": __dirname + "/index.html",
        "/*.js": __dirname + "/js",
        "/*.scss": __dirname + "/js"
    },

    deps: {

        production: false,

        uglify: false,
        moduleDeps: {

            transform: [['babelify', {sourceMap: false, stage: 0, optional: 'runtime', ignore: ["*.min.js"]}]]
        }
    },

    aliases: {
        "factories": __dirname + '/factories/factories.js',
        "client": __dirname + '/../socket/client/client.js'
    }
};

server.use(compression());
server.use(serve.main(serve_conf));
server.use(serve.scss());
server.listen(servePort);