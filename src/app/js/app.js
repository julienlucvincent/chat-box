/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

import { createElement } from 'react'
import { render } from 'react-dom'
import container from './components/container'

/**
 * Sets the tab title.
 */
document.title = "SOCKET TESTS";

/**
 * Initialize the react application.
 */
window.onload = () => {
    render(createElement(container), document.getElementById("app"))
};
