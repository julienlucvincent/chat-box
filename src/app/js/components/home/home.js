/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

import React from 'react'
import { div, input, button } from 'factories'

export default
class home extends React.Component {

    constructor() {

        super();

        this.state = {
            room: null
        }
    }

    render() {

        return (
            div({className: 'home'},
                div({className: 'splash'},
                    input({
                        type: 'text',
                        placeholder: 'Enter room name',
                        onChange: this._change,
                        value: this.state.room
                    }),
                    button({onClick: this._go}, "Enter Room")
                )
            )
        )
    }

    /**
     * Set the state with the new text area value
     *
     * @param e - the text field event object.
     */
    _change = (e) => {

        /**
         * this.setState() is also a React.Component specific method.
         *
         * It merges the state specified with the current state of the component,
         * and then it CALLS THE RENDER METHOD.
         *
         * This means that every time you call this method, your application will re-render.
         */
        this.setState({
            room: e.target.value
        })
    };

    /**
     * Set the parents room state to this components room state.
     *
     * Transition to the room component.
     *
     * @param e - the button even object.
     */
    _go = (e) => {
        e.preventDefault();

        if (this.state.room && this.state.room != '') {
            this.props.setRoom(this.state.room);
            this.props.goTo('room')
        }
    };
}