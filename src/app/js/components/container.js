/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

import React from 'react'
import { _, div } from 'factories'

import home from './home/home'
import room from './room/room'

/**
 * This class extends React.Component. This means that it now contains
 * all the methods that Component contains.
 */
export default
class container extends React.Component {

    constructor() {

        super();

        this.state = {
            page: home,
            room: null
        }
    }

    /**
     * Render is a method belonging to Component that gets called
     * by the parent of this container class. In this case the render method
     * in app.js is calling this.
     *
     * The render method is where all UI related code will go.
     *
     * In here we are rendering the current page. Initially it is the home.js component,
     * but on entering a room it switches to the room.js component.
     */
    render() {

        return (
            _(this.state.page)(
                /**
                 * lets pass some properties to the component being rendered.
                 *
                 * We pass a javascript object containing two methods and a state.
                 */
                {goTo: this._goTo, setRoom: this._setRoom, room: this.state.room}

            )
        )
    }

    /**
     * Change the page being rendered.
     *
     * @param page - the page you want to go to
     */
    _goTo = (page) => {
        switch (page) {
            case "home":
                return this.setState({
                    page: home
                });
            case "room":
                return this.setState({
                    page: room
                });
            default:
                return this.setState({
                    page: home
                });
        }
    };

    /**
     * Set the room, this data gets passed to whatever page is currently
     * being rendered. The room.js component makes use of the room prop
     * when connecting to the server.
     *
     * @param room - the room you want to join.
     */
    _setRoom = (room) => {
        this.setState({
            room: room
        })
    };
}