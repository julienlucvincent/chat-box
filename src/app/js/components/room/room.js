/*
 |--------------------------------------------------------------------------
 | Created by Julien Vincent
 |--------------------------------------------------------------------------
 **/

import React from 'react'
import { div, input, button, p } from 'factories'

import client from 'client'

export default
class room extends React.Component {

    constructor() {

        super();

        this.state = {
            messages: [],
            chat: null
        }
    }

    /**
     * This is a React.Component lifecycle hook that
     * automatically gets called just before this component
     * gets mounted into the browser DOM.
     */
    componentWillMount() {

        /**
         * Register with the client
         */
        client.register(this._messageReceived);

        /**
         * Connect to the server socket using the room
         * set previously
         */
        client.connect(this.props.room);
    }

    /**
     * The callback to be registered with the client. On receiving
     * a message, this method push the received message to this
     * components state. Calling this.setState() automatically
     * re-renders the component.
     *
     * @param message
     */
    _messageReceived = (message) => {
        this.setState({
            message: this.state.messages.push(message)
        })
    };

    render() {

        return (
            div({className: 'room'},
                div({className: 'splash'},
                    div({className: 'message-box'},
                        this.state.messages.map(message => (
                            p({key: message}, message)
                        ))
                    ),

                    div({className: 'chat-box'},
                        input({
                            type: 'text',
                            placeholder: 'Say something funny as fek...',
                            onChange: this._change,
                            value: this.state.chat
                        }),
                        button({onClick: this._send}, "SAY IT!!")
                    )
                )
            )
        )
    }

    _change = (e) => {
        this.setState({
            chat: e.target.value
        })
    };

    _send = (e) => {
        e.preventDefault();

        client.send(this.state.chat);
        this.setState({
            chat: ''
        })
    }
}
