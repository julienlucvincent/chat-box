# Chat Box Demo App

### usage

+ `git clone https://julienlucvincent@bitbucket.org/julienlucvincent/chat-box.git`
+ `cd chat-box`
+ `npm install`
+ `npm run all`

to run the server and application separately you can use the commands `npm run server` for the socket server and `npm run app` for the web-app